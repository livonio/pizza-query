-- Databricks notebook source
select count(*),        --- Linha não nulas
       count(1),        --- Linha não nulas
       count(idPedido)  --- Linhão não nulas colunas IdPedido
from silver.pizza_query.pedido

-- COMMAND ----------

select count(*)
from silver.pizza_query.pedido
where flKetchup is not null

-- COMMAND ----------

select  descUF,
        count(*) as qtdepedido
from silver.pizza_query.pedido
where descUF != 'São Paulo'
group by descUF
having qtdepedido >= 75
order by qtdepedido desc
limit 6

-- COMMAND ----------

select *
from silver.pizza_query.produto
where descItem like '%abacaxi%'

-- COMMAND ----------

select descUF,
       flKetchup,
       count(*)
from silver.pizza_query.pedido

group by descUF, flKetchup
order by descUF, flKetchup



